-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: mysqldb
-- Gegenereerd op: 06 jan 2021 om 18:55
-- Serverversie: 5.7.32
-- PHP-versie: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bozakdb`
--
CREATE DATABASE IF NOT EXISTS `bozakdb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `bozakdb`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `auth_tokens`
--

DROP TABLE IF EXISTS `auth_tokens`;
CREATE TABLE IF NOT EXISTS `auth_tokens` (
                                             `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                                             `selector` char(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                             `token` char(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                             `users_id` int(10) UNSIGNED NOT NULL,
                                             `expires` datetime DEFAULT NULL,
                                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `auth_tokens`
--



-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `channels`
--

DROP TABLE IF EXISTS `channels`;
CREATE TABLE IF NOT EXISTS `channels` (
                                          `id` int(11) NOT NULL AUTO_INCREMENT,
                                          `name` varchar(50) NOT NULL,
                                          `description` text,
                                          `organisations_id` int(11) NOT NULL,
                                          PRIMARY KEY (`id`,`organisations_id`),
                                          KEY `fk_channels_organisations1_idx` (`organisations_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `channels`
--

INSERT INTO `channels` (`id`, `name`, `description`, `organisations_id`) VALUES
(1, 'klas 1a', 'het kanaal voor klas 1a', 1),
(4, 'pinkels', 'meisjes 1ste en 2e leerjaar', 2),
(5, 'klas 2a', 'klas 2a', 1),
(6, 'sloebers', 'kanaal van sloebers chiro machelen (jongens 6-7j)', 2),
(7, 'niveau 1 ', 'kanaal voor niveau 1', 3),
(8, 'niveau 2', 'kanaal voor niveau 2', 3),
(9, 'niveau 3', 'kanaal voor niveau 3', 3),
(10, 'klas 3a', 'kanaal over klas 3a', 1),
(11, 'speelclub jongens', 'kanaal voor jongens 3 en 4e leerjaar', 2),
(17, 'test', 'qsdf', 4),
(18, 'qsdfqsde', 'e', 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
                                          `id` int(11) NOT NULL AUTO_INCREMENT,
                                          `title` varchar(45) NOT NULL,
                                          `date` datetime NOT NULL,
                                          `text` text,
                                          `filepath` varchar(100) DEFAULT NULL,
                                          `channels_id` int(11) NOT NULL,
                                          `channels_organisations_id` int(11) NOT NULL,
                                          `users_id` int(11) NOT NULL,
                                          PRIMARY KEY (`id`,`channels_id`,`channels_organisations_id`,`users_id`),
                                          KEY `fk_messages_channels1_idx` (`channels_id`,`channels_organisations_id`),
                                          KEY `fk_messages_users1_idx` (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `messages`
--

INSERT INTO `messages` (`id`, `title`, `date`, `text`, `filepath`, `channels_id`, `channels_organisations_id`, `users_id`) VALUES
(1, 'bericht titel', '2021-01-02 15:57:35', 'testbericht', '', 1, 1, 1),
(8, 'dit is een bericht', '2021-01-03 22:37:52', 'dit is de inhoud van het bericht', '', 4, 2, 2),
(9, 'eerste bericht', '2021-01-03 23:45:41', 'joepie', '', 6, 2, 2),
(10, 'chiro', '2021-01-03 23:46:48', 'Volgende zondag (17/01) is het chiro van 13:30 tot 17u', '', 4, 2, 2),
(11, 'tweede bericht', '2021-01-04 00:01:31', '', '', 6, 2, 2),
(12, 'Eerste bericht van niveau 1', '2021-01-04 01:33:46', 'Welkom in dit kanaal ', '', 7, 3, 1),
(13, 'Eerste bericht van niveau 2', '2021-01-04 01:34:16', 'welkom in dit kanaal!', '', 8, 3, 1),
(14, 'welkom in niveau 3!!', '2021-01-04 01:36:22', 'welkom', '/files/messageUploads/14.pdf', 9, 3, 1),
(15, 'uurcontrole', '2021-01-04 13:06:00', 'uur is 14:05', '', 4, 2, 2),
(16, 'het is 14:31', '2021-01-04 14:31:49', '', '', 4, 2, 2),
(17, 'speelclub jongens eerste bericht', '2021-01-04 14:51:34', 'welkom!!', '', 11, 2, 2),
(18, 'gelukkig nieuwjaar!!!', '2021-01-06 14:54:18', 'Gelukkig nieuwjaar aan iedereen in klas 1a!!', '', 1, 1, 1),
(22, 'qsdf', '2021-01-06 16:52:22', '', '', 17, 4, 1),
(23, 'qsdf', '2021-01-06 16:07:07', 'qsdf', '/files/messageUploads/23.pdf', 17, 4, 1),
(24, 'wc', '2021-01-06 19:52:39', 'qdf', '/files/messageUploads/24.pdf', 1, 1, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `organisations`
--

DROP TABLE IF EXISTS `organisations`;
CREATE TABLE IF NOT EXISTS `organisations` (
                                               `id` int(11) NOT NULL AUTO_INCREMENT,
                                               `name` varchar(50) NOT NULL,
                                               `description` text,
                                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `organisations`
--

INSERT INTO `organisations` (`id`, `name`, `description`) VALUES
(1, 'school leiebloem', 'school leiebloem gelegen te machelen zulte'),
(2, 'chiro machelen', 'organisatie van chiro machelen'),
(3, 'Voetbalploeg machelen', 'De organisatie van voetbalploeg kme machelen'),
(4, 'test', 'test organisatie'),
(5, 'aaaaa', 'aaaa');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `organisations_has_users`
--

DROP TABLE IF EXISTS `organisations_has_users`;
CREATE TABLE IF NOT EXISTS `organisations_has_users` (
                                                         `organisations_id` int(11) NOT NULL,
                                                         `users_id` int(11) NOT NULL,
                                                         PRIMARY KEY (`organisations_id`,`users_id`),
                                                         KEY `fk_organisations_has_users_users1_idx` (`users_id`),
                                                         KEY `fk_organisations_has_users_organisations_idx` (`organisations_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `organisations_has_users`
--

INSERT INTO `organisations_has_users` (`organisations_id`, `users_id`) VALUES
(1, 1),
(3, 1),
(4, 1),
(5, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `replies`
--

DROP TABLE IF EXISTS `replies`;
CREATE TABLE IF NOT EXISTS `replies` (
                                         `id` int(11) NOT NULL AUTO_INCREMENT,
                                         `text` text,
                                         `messages_id` int(11) NOT NULL,
                                         `messages_channels_id` int(11) NOT NULL,
                                         `messages_channels_organisations_id` int(11) NOT NULL,
                                         `users_id` int(11) NOT NULL,
                                         PRIMARY KEY (`id`,`messages_id`,`messages_channels_id`,`messages_channels_organisations_id`,`users_id`),
                                         KEY `fk_replies_messages1_idx` (`messages_id`,`messages_channels_id`,`messages_channels_organisations_id`),
                                         KEY `fk_replies_users1_idx` (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
                                       `users_id` int(11) NOT NULL,
                                       `role` enum('owner','user','requested','denied') DEFAULT NULL,
                                       `channels_id` int(11) NOT NULL,
                                       `organisations_id` int(11) NOT NULL,
                                       `description` text,
                                       PRIMARY KEY (`users_id`,`organisations_id`,`channels_id`),
                                       KEY `fk_roles_users1_idx` (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `roles`
--

INSERT INTO `roles` (`users_id`, `role`, `channels_id`, `organisations_id`, `description`) VALUES
(1, 'owner', 0, 1, NULL),
(1, 'owner', 1, 1, 'eigenaar van dit kanaal'),
(1, 'owner', 5, 1, 'Eigenaar van dit kanaal'),
(1, 'owner', 10, 1, 'Eigenaar van dit kanaal'),
(1, 'owner', 12, 1, 'Eigenaar van dit kanaal'),
(1, 'owner', 13, 1, 'Eigenaar van dit kanaal'),
(1, 'owner', 14, 1, 'Eigenaar van dit kanaal'),
(1, 'user', 4, 2, 'papa van marlies'),
(1, 'user', 6, 2, 'papa van marlies'),
(1, 'owner', 0, 3, 'eigenaar van deze organisatie'),
(1, 'owner', 7, 3, 'Eigenaar van dit kanaal'),
(1, 'owner', 8, 3, 'Eigenaar van dit kanaal'),
(1, 'owner', 9, 3, 'Eigenaar van dit kanaal'),
(1, 'owner', 0, 4, 'eigenaar van deze organisatie'),
(1, 'owner', 17, 4, 'Eigenaar van dit kanaal'),
(1, 'owner', 0, 5, 'eigenaar van deze organisatie'),
(2, 'requested', 1, 1, 'aezr'),
(2, 'owner', 0, 2, NULL),
(2, 'owner', 3, 2, 'Eigenaar van dit kanaal'),
(2, 'owner', 4, 2, 'Eigenaar van dit kanaal'),
(2, 'owner', 6, 2, 'Eigenaar van dit kanaal'),
(2, 'owner', 11, 2, 'Eigenaar van dit kanaal'),
(2, 'owner', 18, 2, 'Eigenaar van dit kanaal'),
(2, 'user', 7, 3, 'broer van sander'),
(2, 'user', 8, 3, 'broer van eriek'),
(2, 'user', 9, 3, 'broer van cedric'),
(3, 'user', 4, 2, 'vader van leen');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
                                       `id` int(11) NOT NULL AUTO_INCREMENT,
                                       `first_name` varchar(50) NOT NULL,
                                       `last_name` varchar(50) NOT NULL,
                                       `mail` varchar(100) NOT NULL,
                                       `phone` varchar(20) DEFAULT NULL,
                                       `password` varchar(200) DEFAULT NULL,
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `mail`, `phone`, `password`) VALUES
(1, 'jasper', 'coppens', 'jasper.coppens12@gmail.com', '0479848952', '$2y$10$mnUiHXh9RHu8i2vAH/PwueNQyzdCUkC8pEVMEJb49133HfOVA8qW6'),
(2, 'willem', 'huys', 'willemnhuys@gmail.com', '0478858585', '$2y$10$KsWHfLhQ169SQm8cCdGyD.XEL.wk1TLwiObmoDjPxR6eZKus00Nk6'),
(3, 'arend', 'standaert', 'arendstandaert@gmail.com', '1234567890', '$2y$10$/K9z22Mg2Jt1aPTeUFBWf.vkKcMxtdQdymYLAY1ArM52fDaQFKaku');

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `channels`
--
ALTER TABLE `channels`
    ADD CONSTRAINT `fk_channels_organisations1` FOREIGN KEY (`organisations_id`) REFERENCES `organisations` (`id`);

--
-- Beperkingen voor tabel `messages`
--
ALTER TABLE `messages`
    ADD CONSTRAINT `fk_messages_channels1` FOREIGN KEY (`channels_id`,`channels_organisations_id`) REFERENCES `channels` (`id`, `organisations_id`),
    ADD CONSTRAINT `fk_messages_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `organisations_has_users`
--
ALTER TABLE `organisations_has_users`
    ADD CONSTRAINT `fk_organisations_has_users_organisations` FOREIGN KEY (`organisations_id`) REFERENCES `organisations` (`id`),
    ADD CONSTRAINT `fk_organisations_has_users_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `replies`
--
ALTER TABLE `replies`
    ADD CONSTRAINT `fk_replies_messages1` FOREIGN KEY (`messages_id`,`messages_channels_id`,`messages_channels_organisations_id`) REFERENCES `messages` (`id`, `channels_id`, `channels_organisations_id`),
    ADD CONSTRAINT `fk_replies_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `roles`
--
ALTER TABLE `roles`
    ADD CONSTRAINT `fk_roles_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
