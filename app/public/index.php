<?php
require_once __DIR__ . '/../vendor/autoload.php';

// Create Router instance
$router = new \Bramus\Router\Router();

// General variables
//$basePath = __DIR__ . '/../';
//$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
//$twig = new \Twig\Environment($loader);

$router->setNamespace('\Http');

$router->before('GET|POST', '/.*', function (){
    session_start();
});

// Home route
$router->get('/', 'AuthController@home');

//login en register
$router->get('/register', 'AuthController@showRegister');
$router->post('/register', 'AuthController@register');
$router->get('/login', 'AuthController@showLogin');
$router->post('/login', 'AuthController@login');
$router->get('/logout', 'AuthController@showLogout');
$router->post('/logout', 'AuthController@logout');

//dashboard
$router->get('/dashboard', 'DashboardController@overview');

//detail pagina van een organisatie
//in het geval van owner: hier zie je opties om channels te verwijderen en bij te maken
//in het geval van user:Hier zie je alle channels van de organisatie waarop je toegang hebt
$router->get('/dashboard/organisations/(\d+)/channels', 'OrganisationController@overviewOrganisation');

//redirect
$router->get('/dashboard/organisations', function (){
    header('Location: /dashboard');
    exit();
});

//detail pagina van een channel
$router->get('/dashboard/organisations/(\d+)/channels/(\d+)/messages', 'OrganisationController@showChannel');

//add en delete channel (voor owners van een organisatie)
$router->get('/dashboard/organisations/(\d+)/create-channel', 'OrganisationController@showCreateChannel');
$router->post('/dashboard/organisations/(\d+)/create-channel', 'OrganisationController@createChannel');
$router->get('/dashboard/organisations/(\d+)/channels/(\d+)/delete-channel', 'OrganisationController@showDeleteChannel');
$router->post('/dashboard/organisations/(\d+)/channels/(\d+)/delete-channel', 'OrganisationController@deleteChannel');

//als owner zie je hier wie er toegang gevraagd heeft tot een kanaal en kan je toegang geven
$router->get('/dashboard/organisations/(\d+)/channels/(\d+)/requests', 'OrganisationController@showRequests');
$router->post('/dashboard/organisations/(\d+)/channels/(\d+)/requests', 'OrganisationController@requests');

//add message (voor owners van een organisatie)
$router->get('/dashboard/organisations/(\d+)/channels/(\d+)/create-message', 'OrganisationController@showCreateMessage');
$router->post('/dashboard/organisations/(\d+)/channels/(\d+)/create-message', 'OrganisationController@createMessage');

//delete a message (voor owners van een organisatie)
$router->get('/dashboard/organisations/(\d+)/channels/(\d+)/messages/(\d+)/delete-message', 'OrganisationController@showDeleteMessage');
$router->post('/dashboard/organisations/(\d+)/channels/(\d+)/messages/(\d+)/delete-message', 'OrganisationController@deleteMessage');

//toegang vragen tot een kanaal aan de eigenaar
$router->get('/dashboard/search-organisations/(\d+)/show-channels/(\d+)/request-permission', 'OrganisationController@showRequestPermission');
$router->post('/dashboard/search-organisations/(\d+)/show-channels/(\d+)/request-permission', 'OrganisationController@requestPermission');

//verzoek toestaan
$router->get('/dashboard/organisations/(\d+)/channels/(\d+)/requests/allow', 'OrganisationController@requestAllow');

//verzoek weigeren
$router->get('/dashboard/organisations/(\d+)/channels/(\d+)/request-deny', 'OrganisationController@requestDeny');

//unsubscribe
$router->get('/dashboard/organisations/(\d+)/channels/(\d+)/unsubscribe', 'OrganisationController@unsubscribe');

//nieuwe organisatie aanmaken
$router->get('/dashboard/create-organisation', 'OrganisationController@showCreateOrganisation');
$router->post('/dashboard/create-organisation', 'OrganisationController@createOrganisation');

//organisatie bewerken
$router->get('/dashboard/organisations/(\d+)/edit-organisation', 'OrganisationController@showEditOrganisation');
$router->post('/dashboard/organisations/(\d+)/edit-organisation', 'OrganisationController@editOrganisation');

//organisaties zoeken
$router->get('/dashboard/search-organisations', 'OrganisationController@showSearchOrganisation');
$router->post('/dashboard/search-organisations', 'OrganisationController@searchOrganisation');

//organisaties zoeken dan naar kanalen gaan
$router->get('/dashboard/search-organisations/(\d+)/show-channels', 'OrganisationController@showAllChannelsFromSearchedOrg');

// Run it!
$router->run();