<?php

namespace Http;
use Services\DatabaseConnector;
use User;

class AuthController
{
    protected  \Doctrine\DBAL\Connection $db;
    protected \twig\Environment $twig;
    protected User $user;

    public function __construct()
    {
        // initiate DB connection
        $this->db = DatabaseConnector::getConnection();

        // bootstrap Twig
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../../resources/templates');
        $this->twig = new \Twig\Environment($loader);
    }

    //source for user authentication: https://stackoverflow.com/questions/3128985/php-login-system-remember-me-persistent-cookie?fbclid=IwAR0K3NDrtJsMlgeVSWGME_IMZnffwxbQQxGDnq1O21lbsx4XJ-7qoBgRPjY
    public function home()
    {
        if (empty($_SESSION['userid']) && !empty($_COOKIE['remember'])) {
            list($selector, $authenticator) = explode(':', $_COOKIE['remember']);

            $stmt = $this->db->prepare("SELECT * FROM auth_tokens WHERE selector = ?");
            $stmt->execute([$selector]);
            $row = $stmt ->fetchAssociative();

            if (hash_equals($row['token'], hash('sha256', base64_decode($authenticator)))) {
                $_SESSION['userid'] = $row['users_id'];

                $stmt = $this->db->prepare('SELECT * FROM users WHERE id = ?');
                $stmt->execute([$row['users_id']]);
                $user = $stmt->fetchAssociative();
                $newUser = User::constructWithIdFNameLNameMailPhone($user['id'], $user['first_name'], $user['last_name'], $user['mail'], $user['phone'] );
                $_SESSION['user'] = $newUser;
            }
        }

        $authenticated = false;
        if(isset($_SESSION['user'])){
            $authenticated = true;
        }
        echo $this->twig->render('pages/index.twig',[
            'authenticated' => $authenticated
        ]);
    }

    public function showLogin()
    {
        //al ingelogd:
        if(isset($_SESSION['user'])){
            header('location: /');
            exit();
        }
        echo $this->twig->render('/pages/login.twig',[]);
    }

    public function login(){
        //General variables
        $errorMail='';
        $errorPassword='';
        $errors = false;

        //Formcheck
        //check for whitespace and mail validation from w3c: https://www.w3schools.com/php/php_form_url_email.asp
        if (isset($_POST['btnSubmit'])) {
            $password = $_POST['password'];

            if (!$_POST['mail']) {
                $errorMail = 'Mail is required!';
                $errors = true;
            }

            if (!$password){
                $errorPassword='Password is required!';
                $errors = true;
            }

            if(!$errors){
                $stmt = $this->db->prepare('SELECT * FROM users WHERE mail = ?');
                $stmt->execute([$_POST['mail']]);
                $user = $stmt->fetchAssociative();

                if (($user !== false) && (password_verify($password, $user['password']))) {
                    $selector = base64_encode(random_bytes(9));
                    $authenticator = random_bytes(33);

                    setcookie(
                        'remember',
                        $selector.':'.base64_encode($authenticator),
                        time() + 864000
                    );

                    $stmtInsert = $this->db->prepare(' INSERT INTO auth_tokens (selector, token, users_id, expires) VALUES (?, ?, ?, ?)');
                    $stmtInsert->bindValue(1, $selector);
                    $stmtInsert->bindValue(2, hash('sha256', $authenticator));
                    $stmtInsert->bindValue(3, $user['id'] );
                    $stmtInsert->bindValue(4, date('Y-m-d\TH:i:s', time() + 864000));
                    $stmtInsert->execute();

                    $newUser = User::constructWithIdFNameLNameMailPhone($user['id'], $user['first_name'], $user['last_name'], $user['mail'], $user['phone'] );
                    $_SESSION['user'] = $newUser;
                    $_SESSION['userid'] = $newUser->getId();

                    header('location: /dashboard');
                    exit();
                }
                else {
                    $errorMail = 'Invalid password or mail';
                    $errorPassword = 'invalid password or mail';
                }
            }
        }

        //view
        echo $this->twig->render('/pages/login.twig',[
            'errorMail' => $errorMail,
            'errorPassword' => $errorPassword,
            'mail' => $_POST['mail'] ?? '',
        ]);
    }

    public function showRegister()
    {
        //al ingelogd:
        if(isset($_SESSION['user'])){
            header('location: /');
            exit();
        }

        echo $this->twig->render('/pages/register.twig');
    }

    public function register(){

        //General variables
        $errorFName='';
        $errorLName='';
        $errorMail='';
        $errorPhone='';
        $errorPassword='';
        $errorRepeatPassword='';
        $errors = false;

        //Formcheck
        //check for whitespace and mail validation from w3c: https://www.w3schools.com/php/php_form_url_email.asp
        if (isset($_POST['btnSubmit'])) {
            $password = $_POST['password'];

            if (!$_POST['fname']) {
                $errorFName = 'First name is required!';
                $errors = true;
            }elseif (!preg_match("/^[a-zA-Z-' ]*$/",$_POST['fname'])) {
                $errorFName = "Only letters and white space allowed";
                $errors = true;
            }
            if (!$_POST['lname']) {
                $errorLName = 'Last name is required!';
                $errors = true;
            }elseif (!preg_match("/^[a-zA-Z-' ]*$/",$_POST['lname'])) {
                $errorLName = "Only letters and white space allowed";
                $errors = true;
            }

            if (!$_POST['mail']) {
                $errorMail = 'Mail is required!';
                $errors = true;
            }elseif (!filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
                $errorMail = "Invalid email format";
                $errors = true;
            }

            $stmt = $this->db->prepare('SELECT * FROM users WHERE mail = ?');
            $stmt->execute([$_POST['mail']]);
            $mail = $stmt->fetchAllAssociative();

            if(count($mail) !== 0){
                $errors = true;
                $errorMail = 'user with that email already exists';
            }

            if (!$_POST['phone']) {
                $errorPhone = 'Phone number is required!';
                $errors = true;
            }elseif (strlen($_POST['phone']) != 10 ){
                $errorPhone = 'Phone number has to be a number with 10 digits, following format 0471 23 45 67';
                $errors = true;
            }

            // Validate password strength
            $uppercase = preg_match('@[A-Z]@', $password);
            $lowercase = preg_match('@[a-z]@', $password);
            $number    = preg_match('@[0-9]@', $password);
            $specialChars = preg_match('@[^\w]@', $password);

            if (!$password){
                $errorPassword='Password is required!';
                $errors = true;
            }elseif (!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8){
                $errorPassword = 'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.';
                $errors = true;
            }

            if (!$_POST['repeatPassword']){
                $errorRepeatPassword = 'Repeated password is required!';
                $errors = true;
            }elseif (strcmp($password, $_POST['repeatPassword']) !== 0){
                $errorPassword = $errorRepeatPassword = 'passwords did not match';
                $errors = true;
            }

            if(!$errors){
                $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
                $stmtInsert = $this->db->prepare(' INSERT INTO users(first_name, last_name, mail, phone, password) VALUES(?,?,?,?,?)');
                $stmtInsert->bindValue(1, $_POST['fname'], 'string');
                $stmtInsert->bindValue(2, $_POST['lname'], 'string');
                $stmtInsert->bindValue(3, $_POST['mail'], 'string');
                $stmtInsert->bindValue(4, $_POST['phone'], 'string');
                $stmtInsert->bindValue(5, $hashedPassword, 'string');
                $stmtInsert->execute();

                //Redirect if succesfull
                header('Location: /');
                exit();
            }
        }

        //View
        echo $this->twig->render('/pages/register.twig', [
            'fname' => $_POST['fname'] ?? '',
            'lname' => $_POST['lname'] ?? '',
            'mail' => $_POST['mail'] ?? '',
            'phone' => $_POST['phone'] ?? '',
            'errorFName' => $errorFName,
            'errorLName' => $errorLName,
            'errorMail' => $errorMail,
            'errorPhone' => $errorPhone,
            'errorPassword' => $errorPassword,
            'errorRepeatPassword' => $errorRepeatPassword

        ]);
    }

    public function showLogout()
    {
       $this->logout();
    }

    public function logout()
    {
        $_SESSION = [];

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        session_destroy();
        header('location: /login');
        exit();
    }

}