<?php

namespace Http;

use DateTime;
use Services\DatabaseConnector;
use Organisation;
use Channel;
use User;
use SplFileInfo;

class OrganisationController
{
    protected \Doctrine\DBAL\Connection $db;
    protected \Twig\Environment $twig;
    protected User $user;

    public function __construct()
    {
        //reroute if not logged in
        if(! isset($_SESSION['user'])){
            header('location: /login');
            exit();
        }

        //user aanmaken
        $this->user = $_SESSION['user'];

        // initiate DB connection
        $this->db = DatabaseConnector::getConnection();

        // bootstrap Twig
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../../resources/templates');
        $this->twig = new \Twig\Environment($loader);
    }
    //parameter tempering in orde
    public function overviewOrganisation($id)
    {

        $stmt = $this->db->prepare('SELECT * FROM organisations WHERE `id` = ?');
        $stmt->execute([$id]);
        $organisationsFromDb = $stmt->fetchAllAssociative();

        // Data
        $stmtIsOwner = $this->db->prepare('SELECT roles.role 
                                                        FROM organisations 
                                                        CROSS JOIN users 
                                                        LEFT JOIN roles ON roles.users_id = users.id 
                                                        LEFT JOIN channels ON channels.organisations_id = organisations.id
                                                        WHERE (roles.users_id = ?)
                                                        AND (users.id = roles.users_id)
                                                        AND (roles.organisations_id = ?)
                                                        AND (roles.channels_id = 0 )');
        $stmtIsOwner->execute([$this->user->getId(), $id]);
        $owner = $stmtIsOwner->fetchOne();

        $channelsFromDb = [];
        $isOwner = false;
        if($owner == 'owner'){
            $isOwner = true;

            $stmt = $this->db->prepare('SELECT channels.id, channels.name, channels.description, channels.organisations_id
                                        FROM channels 
                                        LEFT JOIN organisations ON channels.organisations_id = organisations.id 
                                        WHERE organisations.id = ? 
                                        ORDER BY channels.name');
            $stmt->execute([$id]);
            $channelsFromDb = $stmt->fetchAllAssociative();
        }else{
            //
            $stmt = $this->db->prepare('SELECT channels.id, channels.name, channels.description, channels.organisations_id
                                        FROM channels 
                                        LEFT JOIN organisations ON channels.organisations_id = organisations.id 
                                        LEFT JOIN roles ON roles.channels_id = channels.id
                                        WHERE organisations.id = ? 
                                        AND roles.role = \'user\'
                                        AND roles.channels_id = channels.id
                                        AND roles.users_id = ?
                                        ORDER BY channels.name');
            $stmt->execute([$id, $this->user->getId()]);
            $channelsFromDb = $stmt->fetchAllAssociative();

            //geen toegang tot een kanaal: redirect
            if(count($channelsFromDb) === 0){
                header('Location: /dashboard');
                exit();
            }
        }


        //@TODO efficienter maken voor één resultaat.
        $organisations = null;
        foreach ($organisationsFromDb as $organisationFromDb) {
            $organisations = Organisation::constructWitIdNameDescriptionIsOwner($organisationFromDb['id'], $organisationFromDb['name'], $organisationFromDb['description'], $isOwner);
        }

        $channels = [];
        foreach ($channelsFromDb as $channelFromDb){
            $newChannel = new Channel($channelFromDb['id'], $channelFromDb['name'], $channelFromDb['description'], $channelFromDb['organisations_id']);
            array_push($channels, $newChannel);
        }

        if (!$id) {
            header('Location: /dashboard');
            exit();
        }

        // View
        echo $this->twig->render('pages/view-organisation.twig', [
            'organisations' => $organisations,
            'channels' => $channels,
            'authenticated' => true
        ]);
    }
    //parameter tempering in orde
    public function showChannel($orgId, $channelId)
    {
        //check voor toegang
        $role = $this->db->fetchAssociative('SELECT roles.role FROM roles WHERE roles.users_id = ? AND organisations_id = ? AND channels_id = ?', [$this->user->getId(), $orgId, $channelId]);
        if(!$role){
            header('Location: /dashboard/' );
            exit();
        }

        $stmt = $this->db->prepare('SELECT * FROM organisations WHERE `id` = ?');
        $stmt->execute([$orgId]);
        $organisationFromDb = $stmt->fetchAssociative();

        $stmt = $this->db->prepare('SELECT * FROM `channels` WHERE channels.id = ?');
        $stmt->execute([$channelId]);
        $channelFromDb = $stmt->fetchAssociative();

        $stmt = $this->db->prepare('SELECT users.first_name, users.last_name, messages.id, messages.text, messages.title, messages.date, messages.filepath
                                        FROM messages LEFT JOIN channels ON messages.channels_id = channels.id LEFT JOIN users ON messages.users_id = users.id 
                                        WHERE channels.id = ? ORDER BY messages.date DESC');
        $stmt->execute([$channelId]);
        $messagesFromDb = $stmt->fetchAllAssociative();

        $noMessages = '';

        if (!$messagesFromDb) {
            $noMessages = "dit kanaal heeft nog geen berichten geplaatst";
        }

        //is de huidige gebruiker eigenaar van de organisatie?
        $stmtIsOwner = $this->db->prepare('SELECT roles.role 
                                                        FROM organisations 
                                                        CROSS JOIN users 
                                                        LEFT JOIN roles ON roles.users_id = users.id 
                                                        WHERE (organisations.id = roles.organisations_id) 
                                                        AND (users.id = roles.users_id) 
                                                        AND (roles.users_id = ?)
                                                        AND (roles.organisations_id = ?)');
        $stmtIsOwner->execute([$this->user->getId(), $orgId]);
        $owner = $stmtIsOwner->fetchOne();

        $isOwner = false;
        if($owner == 'owner'){
            $isOwner = true;
        }

        $organisation = Organisation::constructWitIdNameDescriptionIsOwner($organisationFromDb['id'], $organisationFromDb['name'], $organisationFromDb['description'], $isOwner);
        $channel = new Channel($channelFromDb['id'], $channelFromDb['name'], $channelFromDb['description'], $channelFromDb['organisations_id'], );

        // View
        echo $this->twig->render('pages/view-messages.twig', [
            'messages' => $messagesFromDb,
            'channel' => $channel,
            'noMessages' => $noMessages,
            'authenticated' => true,
            'organisation' => $organisation
        ]);
    }

    public function showSearchOrganisation()
    {
        $formErrors = isset($_SESSION['flash']['formErrors']) ? $_SESSION['flash']['formErrors'] : '';
        $term = isset($_SESSION['flash']['term']) ? $_SESSION['flash']['term'] : '';
        unset($_SESSION['flash']);

        $organisationsRow = $this->db->fetchAllAssociative('select * From organisations order by name', []);

        foreach ($organisationsRow as $organisationRow) {
            $organisations[] = Organisation::constructWitIdNameDescription($organisationRow['id'], $organisationRow['name'], $organisationRow['description']);
        }


        if ($term !== ''){
            $organisations = $this->db->fetchAllAssociative('select * From organisations WHERE `name` LIKE ? order by name', ['%' . $term . '%']);
        }

        echo $this->twig->render('pages/search-organisation.twig', [
            'formErrors' => $formErrors,
            'organisations' => $organisations,
            'searchTerm' => $term,
            'authenticated' => true
        ]);
    }

    public function searchOrganisation()
    {
        $term = isset($_POST['term']) ? trim($_POST['term']) : '';

        if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'search-organisation')) {
            if ($term !== '') {
                if (!preg_match('/^[a-zA-Z0-9_]+$/', $term)) {
                    $formErrors = 'Dit is geen geldige zoekopdracht';
                }
            }

            if (! $formErrors) {
                $_SESSION['flash'] = ['term' => $term];
                header('Location: /dashboard/search-organisations' );
                exit();
            }
        }

        $_SESSION['flash'] = ['formErrors' => $formErrors,
            'term' => $term];
        header('Location: /dashboard/search-organisations' );
        exit();
    }

    public function showAllChannelsFromSearchedOrg($id)
    {
        $stmt = $this->db->prepare('SELECT * FROM organisations WHERE `id` = ?');
        $stmt->execute([$id]);
        $organisations = $stmt->fetchAllAssociative();

        $stmt = $this->db->prepare('SELECT channels.id, channels.name, channels.description, channels.organisations_id
                                        FROM channels     
                                        WHERE organisations_id = ? 
                                        ORDER BY channels.name');
        $stmt->execute([$id]);
        $channelsFromDb = $stmt->fetchAllAssociative();


        //@TODO efficienter maken voor één resultaat.
        $organisation = null;
        foreach ($organisations as $organisationFromDb) {
            $organisation = Organisation::constructWitIdNameDescription($organisationFromDb['id'], $organisationFromDb['name'], $organisationFromDb['description']);
        }

        $channels = [];
        foreach ($channelsFromDb as $channel){
            $newChannel = new Channel($channel['id'], $channel['name'], $channel['description'], $channel['organisations_id']);
            array_push($channels, $newChannel);
        }

        if (!$id) {
            header('Location: /dashboard');
            exit();
        }

        // View
        echo $this->twig->render('pages/viewChannelsFromSrchOrg.twig', [
            'organisations' => $organisation,
            'channels' => $channels,
            'authenticated' => true
        ]);
    }

    public function showCreateOrganisation()
    {
        echo $this->twig->render('pages/add-organisation.twig',[
            'authenticated' => true
        ]);
    }

    public function createOrganisation()
    {
        //General variables
        $errorName = '';
        $errorDescription = '';
        $errorPassword = '';
        $errors = false;

        $description = NULL;
        //Formcheck
        if (isset($_POST['btnSubmit'])) {
            $password = $_POST['password'];

            if (!$_POST['name']) {
                $errorName = 'Naam is een verplicht veld!';
                $errors = true;
            } elseif (strlen($_POST['name']) > 50) {
                $errorName = 'Naam is te lang, gebruik 50 karakters max.';
            }

            if (!$password) {
                $errorPassword = 'Wachtwoord is een verplicht veld!';
                $errors = true;
            }

            $stmt = $this->db->prepare('SELECT * FROM users WHERE id = ?');
            $stmt->execute([$this->user->getId()]);
            $user = $stmt->fetchAssociative();

            if (($user === false) || !(password_verify($password, $user['password']))) {
                $errorPassword = 'Fout wachtwoord';
                $errors = true;
            }

            $description = (array_key_exists('description', $_POST)) ? $_POST['description'] : NULL;

            if (!$errors) {
                $stmtInsertOrg = $this->db->prepare(' INSERT INTO organisations(name, description) VALUES(?,?)');
                $stmtInsertOrg->execute([$_POST['name'], $description]);

                $lastId = $this->db->lastInsertId();

                $stmtInsertOrghasUsers = $this->db->prepare('INSERT INTO organisations_has_users(organisations_id, users_id) VALUES(?,?)');
                $stmtInsertOrghasUsers->execute([$lastId, $this->user->getId()]);

                $stmtInsertRoles = $this->db->prepare('INSERT INTO roles(users_id, role, organisations_id, channels_id, description) VALUES(?,?,?,?,?)');
                $stmtInsertRoles->execute([$this->user->getId(), 'owner', $lastId, 0, 'eigenaar van deze organisatie']);

                //Redirect if succesfull
                header('Location: /dashboard');
                exit();
            }
        }

        //View
        echo $this->twig->render('pages/add-organisation.twig', [
            'name' => $_POST['name'] ?? '',
            'description' => $_POST['description'] ?? '',
            'errorName' => $errorName,
            'errorDescription' => $errorDescription,
            'errorPassword' => $errorPassword,
            'authenticated' => true
        ]);
    }
    //parameter tempering in orde
    public function showEditOrganisation($orgId)
    {
        $organisationFromDB = $this->db->fetchAssociative('SELECT organisations.name, organisations.description, organisations.id 
                                                                    FROM organisations CROSS JOIN users
                                                                    LEFT JOIN roles ON roles.users_id = users.id
                                                                    WHERE (organisations.id = roles.organisations_id)
                                                                    AND (users.id = roles.users_id) AND (roles.role = \'owner\') 
                                                                    AND (roles.users_id = ?) AND (organisations.id = ?)', [$this->user->getId(), $orgId]);


        if ($organisationFromDB === false){
            header('Location: /dashboard');
            exit();
        }
        $organisation = Organisation::constructWitIdNameDescription($organisationFromDB['id'], $organisationFromDB['name'], $organisationFromDB['description']);
        //  @TODO user moet toegang hebben AND ROLE = ...
        echo $this->twig->render('pages/edit-organisation.twig',[
            'name' => $organisation->getName(),
            'description' => $organisation->getDescription(),
            'authenticated' => true
        ]);
    }

    public function editOrganisation($orgId)
    {
        //General variables
        $errorName = '';
        $errorDescription = '';
        $errorPassword = '';
        $errors = false;

        $description = NULL;
        //Formcheck
        if (isset($_POST['btnSubmit'])) {
            $password = $_POST['password'];

            if (!$_POST['name']) {
                $errorName = 'Naam is een verplicht veld!';
                $errors = true;
            } elseif (strlen($_POST['name']) > 50) {
                $errorName = 'Naam is te lang, gebruik 50 karakters max.';
            }

            if (!$password) {
                $errorPassword = 'Wachtwoord is een verplicht veld!';
                $errors = true;
            }

            $stmt = $this->db->prepare('SELECT * FROM users WHERE id = ?');
            $stmt->execute([$this->user->getId()]);
            $user = $stmt->fetchAssociative();

            if (($user === false) || !(password_verify($password, $user['password']))) {
                $errorPassword = 'Fout wachtwoord';
                $errors = true;
            }

            $description = (array_key_exists('description', $_POST)) ? $_POST['description'] : NULL;

            if (!$errors) {
                $stmtUpdateOrg = $this->db->prepare(' UPDATE organisations SET name = ?, description = ? where id = ?');
                $stmtUpdateOrg->execute([$_POST['name'], $description, $orgId]);

                //Redirect if succesfull
                header('Location: /dashboard');
                exit();
            }
        }

        //View
        echo $this->twig->render('pages/edit-organisation.twig', [
            'name' => $_POST['name'] ?? '',
            'description' => $_POST['description'] ?? '',
            'errorName' => $errorName,
            'errorDescription' => $errorDescription,
            'errorPassword' => $errorPassword,
            'authenticated' => true
        ]);
    }
    //parameter tempering in orde
    public function showCreateChannel($orgId)
    {
        //check voor toegang
        $role = $this->db->fetchAssociative('SELECT roles.role FROM roles WHERE roles.users_id = ? AND organisations_id = ? AND channels_id = 0', [$this->user->getId(), $orgId]);
        if(!$role){
            header('Location: /dashboard/' );
            exit();
        }

        // View
        echo $this->twig->render('pages/create-channel.twig', [
            'authenticated' => true
        ]);
    }

    public function createChannel($organisationId)
    {
        //General variables
        $errorName = '';
        $errorDescription = '';
        $errorPassword = '';
        $errors = false;

        $description = NULL;
        //Formcheck
        if (isset($_POST['btnSubmit'])) {
            $password = $_POST['password'];

            if (!$_POST['name']) {
                $errorName = 'Naam is een verplicht veld!';
                $errors = true;
            } elseif (strlen($_POST['name']) > 50) {
                $errorName = 'Naam is te lang, gebruik 50 karakters max.';
            }

            if (!$password) {
                $errorPassword = 'Wachtwoord is een verplicht veld!';
                $errors = true;
            }

            $stmt = $this->db->prepare('SELECT * FROM users WHERE id = ?');
            $stmt->execute([$this->user->getId()]);
            $user = $stmt->fetchAssociative();

            if (($user === false) || !(password_verify($password, $user['password']))) {
                $errorPassword = 'Fout wachtwoord';
                $errors = true;
            }

            $description = (array_key_exists('description', $_POST)) ? $_POST['description'] : NULL;

            if (!$errors) {

                $stmtInsertChannel = $this->db->prepare(' INSERT INTO channels(name, description, organisations_id) VALUES(?,?,?)');
                $stmtInsertChannel->execute([$_POST['name'], $description, $organisationId]);

                $lastId = $this->db->lastInsertId();

                $stmtInsertRole = $this->db->prepare(' INSERT INTO roles(users_id, role, channels_id, organisations_id, description) VALUES(?,?,?,?,?)');
                $stmtInsertRole->execute([$this->user->getId(), 'owner', $lastId, $organisationId, 'Eigenaar van dit kanaal']);

                //Redirect if succesfull
                header('Location: /dashboard/organisations/'. $organisationId . "/channels");
                exit();
            }
        }

        //View
        echo $this->twig->render('pages/create-channel.twig', [
            'name' => $_POST['name'] ?? '',
            'description' => $_POST['description'] ?? '',
            'errorName' => $errorName,
            'errorDescription' => $errorDescription,
            'errorPassword' => $errorPassword,
            'authenticated' => true
        ]);
    }

    public function showDeleteChannel($orgId, $channelId)
    {
        $organisationFromDB = $this->db->fetchAssociative('SELECT organisations.name, organisations.description, organisations.id 
                                                                    FROM organisations CROSS JOIN users
                                                                    LEFT JOIN roles ON roles.users_id = users.id
                                                                    WHERE (organisations.id = roles.organisations_id)
                                                                    AND (users.id = roles.users_id) AND (roles.role = \'owner\') 
                                                                    AND (roles.users_id = ?) AND (organisations.id = ?)', [$this->user->getId(), $orgId]);

        $channelFromDB = $this->db->fetchAssociative('SELECT * FROM `channels` WHERE channels.id = ?', [$channelId]);

        if ($organisationFromDB === false || $channelFromDB === false){
            header('Location: /dashboard/organisations/'. $orgId . "/channels");
            exit();
        }

        $organisation = Organisation::constructWitIdNameDescription($organisationFromDB['id'], $organisationFromDB['name'], $organisationFromDB['description']);
        $channel = new Channel($channelFromDB['id'], $channelFromDB['name'], $channelFromDB['description'], $channelFromDB['organisations_id']);
        echo $this->twig->render('pages/delete-channel.twig', [
            'organisation' => $organisation,
            'channel' => $channel,
            'authenticated' => true
        ]);
    }

    public function deleteChannel($orgId, $channelId)
    {
        $organisationFromDB = $this->db->fetchAssociative('SELECT organisations.name, organisations.description, organisations.id 
                                                                    FROM organisations CROSS JOIN users
                                                                    LEFT JOIN roles ON roles.users_id = users.id
                                                                    WHERE (organisations.id = roles.organisations_id)
                                                                    AND (users.id = roles.users_id) AND (roles.role = \'owner\') 
                                                                    AND (roles.users_id = ?) AND (organisations.id = ?)', [$this->user->getId(), $orgId]);
        if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'delete') && $organisationFromDB !== false) {
            //delete all messages in channel
            $stmt = $this->db->prepare('DELETE FROM messages WHERE channels_id = ?');
            $stmt->execute([$channelId]);

            //delete all roles
            $stmt = $this->db->prepare('DELETE FROM roles WHERE channels_id = ?');
            $stmt->execute([$channelId]);

            //delete channel
            $stmt = $this->db->prepare('DELETE FROM channels WHERE id = ?');
            $stmt->execute([$channelId]);
            header('Location: /dashboard/organisations/'. $orgId . "/channels");
            exit();

        } else{
            if ($organisationFromDB === false){
                header('Location: /dashboard/organisations/'. $orgId . "/channels");
                exit();
            }
        }
    }

    //parameter tempering in orde
    public function showCreateMessage($orgId, $channelId)
    {
        //check voor toegang
        $role = $this->db->fetchAssociative('SELECT roles.role FROM roles WHERE roles.users_id = ? AND organisations_id = ? AND channels_id = ?', [$this->user->getId(), $orgId, $channelId]);

        if($role['role'] !== 'owner'){
            header('Location: /dashboard/' );
            exit();
        }

        // View
        echo $this->twig->render('pages/create-message.twig', [
            'authenticated' => true
        ]);
    }

    public function CreateMessage($orgId, $channelId)
    {
        //General variables
        $errorTitle = '';
        $errorText = '';
        $errorPassword = '';
        $errorDocument = '';
        $errors = false;
        $test = '';

        //Formcheck
        if (isset($_POST['btnSubmit'])) {
            $password = $_POST['password'];

            if (!$_POST['title']) {
                $errorTitle = 'Bericht Titel is een verplicht veld!';
                $errors = true;
            } elseif (strlen($_POST['title']) > 45) {
                $errorTitle = 'Titel is te lang, gebruik 45 karakters max.';
            }

            if (!$password) {
                $errorPassword = 'Wachtwoord is een verplicht veld!';
                $errors = true;
            }

            //verify password
            $stmt = $this->db->prepare('SELECT * FROM users WHERE id = ?');
            $stmt->execute([$this->user->getId()]);
            $user = $stmt->fetchAssociative();

            if (($user === false) || !(password_verify($password, $user['password']))) {
                $errorPassword = 'Fout wachtwoord';
                $errors = true;
            }

            $text = (array_key_exists('text', $_POST)) ? $_POST['text'] : NULL;

            //upload check
            $filepath = '';

            //laatste id vinden van messages
            $stmtId = $this->db->prepare(' SELECT MAX(id) FROM messages;');
            $stmtId->execute();
            $lastId = $stmtId->fetchOne();
            $idNew = $lastId + 1;

            if (isset($_FILES['document']) && ($_FILES['document']['error'] === UPLOAD_ERR_OK)) {

                if (in_array(
                    strtolower((new SplFileInfo($_FILES['document']['name']))->getExtension()),
                    ['pdf']
                )) {
                    $moved = @move_uploaded_file(
                        $_FILES['document']['tmp_name'],
                        __DIR__ . '/../../public/files/messageUploads/' . $idNew. '.pdf'
                    );

                    $filepath = '/files/messageUploads/' . $idNew . '.pdf';
                    $test = $filepath;
                    if (!$moved) {
                        $errors = true;
                         $errorDocument = "Error tijdens opslaan";
                    }
                } else {
                    $errors = true;
                    $errorDocument = "Alleen pdf bestanden toegelaten";
                }
            }

            if (!$errors) {
                $now = new DateTime();
                $now->modify('+1 hour');
                $new_time = $now->format('Y-m-d H:i:s');

                $stmtInsertMessages = $this->db->prepare(' INSERT INTO messages(title, date, text, filepath, channels_id,channels_organisations_id, users_id) VALUES(?,?,?,?,?,?,?)');
                $stmtInsertMessages->execute([$_POST['title'], $new_time,  $text, $filepath, $channelId, $orgId, $this->user->getId() ]);

                //Redirect if succesfull
                header('Location: /dashboard/organisations/'. $orgId. '/channels/' . $channelId . "/messages");
                exit();
            }
        }

        //View
        echo $this->twig->render('pages/create-message.twig', [
            'title' => $_POST['title'] ?? '',
            'text' => $_POST['text'] ?? '',
            'errorTitle' => $errorTitle,
            'errorText' => $errorText,
            'errorPassword' => $errorPassword,
            'errorDocument' => $errorDocument,
            'authenticated' => true,
            'test' => $test
        ]);
    }
    //parameter tempering in orde
    public function showDeleteMessage($orgId, $channelId, $msgId)
    {
        $organisationFromDB = $this->db->fetchAssociative('SELECT organisations.name, organisations.description, organisations.id 
                                                                    FROM organisations CROSS JOIN users
                                                                    LEFT JOIN roles ON roles.users_id = users.id
                                                                    WHERE (organisations.id = roles.organisations_id)
                                                                    AND (users.id = roles.users_id) AND (roles.role = \'owner\') 
                                                                    AND (roles.users_id = ?) AND (organisations.id = ?)', [$this->user->getId(), $orgId]);

        $channelFromDB = $this->db->fetchAssociative('SELECT * FROM `channels` WHERE channels.id = ?', [$channelId]);
        $messageFromDB = $this->db->fetchAssociative('SELECT * FROM `messages` WHERE messages.id = ?', [$msgId]);

        if ($organisationFromDB === false || $channelFromDB === false || $messageFromDB === false){
            header('Location: /dashboard/organisations/'. $orgId . 'channels/' . $channelId . "/messages");
            exit();
        }

        $organisation = Organisation::constructWitIdNameDescription($organisationFromDB['id'], $organisationFromDB['name'], $organisationFromDB['description']);
        $channel = new Channel($channelFromDB['id'], $channelFromDB['name'], $channelFromDB['description'], $channelFromDB['organisations_id']);
        echo $this->twig->render('pages/delete-message.twig', [
            'organisation' => $organisation,
            'channel' => $channel,
            'message' => $messageFromDB,
            'authenticated' => true
        ]);
    }

    public function deleteMessage($orgId, $channelId, $msgId)
    {
        $organisationFromDB = $this->db->fetchAssociative('SELECT organisations.name, organisations.description, organisations.id 
                                                                    FROM organisations CROSS JOIN users
                                                                    LEFT JOIN roles ON roles.users_id = users.id
                                                                    WHERE (organisations.id = roles.organisations_id)
                                                                    AND (users.id = roles.users_id) AND (roles.role = \'owner\') 
                                                                    AND (roles.users_id = ?) AND (organisations.id = ?)', [$this->user->getId(), $orgId]);
        if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'delete') && $organisationFromDB !== false) {
            $stmt = $this->db->prepare('DELETE FROM messages WHERE id = ?');
            $stmt->execute([$msgId]);
            header('Location: /dashboard/organisations/'. $orgId . '/channels/' . $channelId . "/messages");
            exit();

        } else{
            if ($organisationFromDB === false){
                header('Location: /dashboard/organisations/'. $orgId . '/channels/' . $channelId . "/messages");
                exit();
            }
        }
    }
    //parameter tempering in orde
    public function showRequestPermission($orgId, $channelId)
    {
        $organisationFromDB = $this->db->fetchAssociative('SELECT organisations.name, organisations.description, organisations.id 
                                                                FROM organisations  
                                                                WHERE organisations.id = ?', [$orgId]);

        $channelFromDB = $this->db->fetchAssociative('SELECT * FROM `channels` WHERE channels.id = ?', [$channelId]);

        if ($organisationFromDB === false || $channelFromDB === false){
            header('Location: /dashboard/');
            exit();
        }

        $organisation = Organisation::constructWitIdNameDescription($organisationFromDB['id'], $organisationFromDB['name'], $organisationFromDB['description']);
        $channel = new Channel($channelFromDB['id'], $channelFromDB['name'], $channelFromDB['description'], $channelFromDB['organisations_id']);

        //View
        echo $this->twig->render('pages/request-permission-to-channel.twig', [
            'organisation' => $organisation,
            'channel' => $channel,
            'authenticated' => true
        ]);
    }

    public function requestPermission($orgId, $channelId)
    {
        $errorDescription = '';
        $errorRole = '';
        $errors = false;

        if (isset($_POST['btnSubmit'])) {
            if (!$_POST['description']) {
                $errorDescription = 'Korte beschrijving is een verplicht veld!';
                $errors = true;
            } elseif (strlen($_POST['description']) > 100) {
                $errorDescription = 'Korte beschrijving is te lang, gebruik 100 karakters max.';
                $errors = true;
            }


            $orgRole = $this->db->fetchOne('SELECT roles.role 
                                                        FROM organisations 
                                                        CROSS JOIN users 
                                                        LEFT JOIN roles ON roles.users_id = users.id 
                                                        LEFT JOIN channels ON channels.organisations_id = organisations.id
                                                        WHERE (roles.users_id = ?)
                                                        AND (roles.organisations_id = ?)
                                                        AND (roles.channels_id = 0 )', [$this->user->getId() , $orgId]);

            $role = $this->db->fetchOne('SELECT roles.role 
                                                        FROM organisations 
                                                        CROSS JOIN users 
                                                        LEFT JOIN roles ON roles.users_id = users.id 
                                                        LEFT JOIN channels ON channels.organisations_id = organisations.id
                                                        WHERE (roles.users_id = ?)
                                                        AND (roles.organisations_id = ?)
                                                        AND (roles.channels_id = ? )', [$this->user->getId() , $orgId, $channelId]);
            if($orgRole == 'owner'){
                $errorRole = 'Je bent eigenaar van de overkoepelende organisatie';
            }elseif($role == 'owner'){
                $errorRole = 'Je bent eigenaar van dit kanaal';
            }elseif ($role == 'user'){
                $errorRole = 'Je hebt al toegang tot dit kanaal';
            }elseif ($role == 'requested'){
                $errorRole = 'Je hebt al een verzoek tot toegang gestuurd, even geduld tot de eigenaar dit accepteert';
            }elseif ($role == 'denied'){
                $errorRole = 'Je vorig verzoek is geweigerd';
            }else{
                if (!$errors) {
                    $stmtInsertRequest = $this->db->prepare(' INSERT INTO roles(users_id, role, channels_id, organisations_id, description) VALUES(?,?,?,?,?)');
                    $stmtInsertRequest->execute([$this->user->getId(), 'requested', $channelId, $orgId, $_POST['description']]);
                    header('Location: /dashboard/search-organisations/' . $orgId . '/show-channels');
                    exit();
                }
            }

            $organisationFromDB = $this->db->fetchAssociative('SELECT organisations.name, organisations.description, organisations.id 
                                                                FROM organisations  
                                                                WHERE organisations.id = ?', [$orgId]);

            $channelFromDB = $this->db->fetchAssociative('SELECT * FROM `channels` WHERE channels.id = ?', [$channelId]);

            if ($organisationFromDB === false || $channelFromDB === false){
                header('Location: /dashboard/');
                exit();
            }

            $organisation = Organisation::constructWitIdNameDescription($organisationFromDB['id'], $organisationFromDB['name'], $organisationFromDB['description']);
            $channel = new Channel($channelFromDB['id'], $channelFromDB['name'], $channelFromDB['description'], $channelFromDB['organisations_id']);

            echo $this->twig->render('pages/request-permission-to-channel.twig', [
                'organisation' => $organisation,
                'channel' => $channel,
                'errorDescription' => $errorDescription,
                'errorRole' => $errorRole,
                'authenticated' => true
            ]);
        }
    }
    //parameter tempering in orde
    public function showRequests($orgId, $channelId)
    {
        //check voor toegang
        $role = $this->db->fetchAssociative('SELECT roles.role FROM roles WHERE roles.users_id = ? AND organisations_id = ? AND channels_id = ?', [$this->user->getId(), $orgId, $channelId]);

        if($role['role'] !== 'owner'){
            header('Location: /dashboard/' );
            exit();
        }

        $stmtRequests = $this->db->prepare('SELECT users.id, users.first_name, users.last_name, users.mail, users.phone, roles.description
                                                    FROM organisations 
                                                    CROSS JOIN users 
                                                    LEFT JOIN roles ON roles.users_id = users.id 
                                                    WHERE (organisations.id = roles.organisations_id)
                                                    AND (users.id = roles.users_id) 
                                                    AND (roles.role = \'requested\') 
                                                    AND (roles.channels_id = ?)');

        $stmtRequests->execute([$channelId]);
        $usersFromDb = $stmtRequests->fetchAllAssociative();

        $users = [];
        foreach ($usersFromDb as $user){
            $newUser = User::constructWithIdFNameLNameMailPhoneDescription($user['id'], $user['first_name'], $user['last_name'], $user['mail'], $user['phone'], $user['description'] );
            array_push($users, $newUser);
        }

        //View
        echo $this->twig->render('pages/requests.twig', [
            'users' => $users,
            'orgId' => $orgId,
            'channelId' => $channelId,
            'authenticated' => true
        ]);
    }

    public function requestAllow($orgId, $channelId)
    {
        $stmtAllow = $this->db->prepare('UPDATE roles SET role = \'user\' WHERE (channels_id = ?) AND (organisations_id = ?) AND (users_id = ?)');
        $stmtAllow->execute([$channelId, $orgId, $_GET['userId']]);
        header("Location: /dashboard/organisations/" . $orgId . "/channels/" . $channelId . "/requests");
        exit();
    }

    public function requestDeny($orgId, $channelId)
    {
        $stmtAllow = $this->db->prepare('UPDATE roles SET role = \'denied\' WHERE (channels_id = ?) AND (organisations_id = ?) AND (users_id = ?)');
        $stmtAllow->execute([$channelId, $orgId, $_GET['userId']]);
        header("Location: /dashboard/" . $orgId . "/" . $channelId . "/requests");
        exit();
    }

    public function unsubscribe($orgId, $channelId)
    {
        $stmtAllow = $this->db->prepare('DELETE FROM roles WHERE role = \'user\' AND (channels_id = ?) AND (organisations_id = ?) AND (users_id = ?)');
        $stmtAllow->execute([$channelId, $orgId, $this->user->getId()]);
        header("Location: /dashboard/organisations/" . $orgId . "/channels");
        exit();
    }
}