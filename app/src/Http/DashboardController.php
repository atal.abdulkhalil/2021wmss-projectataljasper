<?php

namespace Http;

use Organisation;
use Services\DatabaseConnector;
use Channel;
use User;

class DashboardController
{
    protected \Doctrine\DBAL\Connection $db;
    protected \Twig\Environment $twig;
    protected User $user;

    public function __construct()
    {
        //reroute if not logged in
        if(! isset($_SESSION['user'])){
            header('location: /login');
            exit();
        }

        //user aanmaken
        $this->user = $_SESSION['user'];

        // initiate DB connection
        $this->db = DatabaseConnector::getConnection();

        // bootstrap Twig
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../../resources/templates');
        $this->twig = new \Twig\Environment($loader);
    }

    public function overview(){
        $myOrganisations = [];
        $mySubscribtions = [];

        //DB connectivity + prepared statements
        $stmtOrganisations = $this->db->prepare('SELECT organisations.name, organisations.description, organisations.id 
                                                    FROM organisations 
                                                    CROSS JOIN users 
                                                    LEFT JOIN roles ON roles.users_id = users.id 
                                                    WHERE (organisations.id = roles.organisations_id)
                                                    AND (users.id = roles.users_id) 
                                                    AND (roles.role = \'owner\') 
                                                    AND (roles.users_id = ?)
                                                    AND (roles.channels_id = 0)');

        $stmtSubscriptions = $this->db->prepare('SELECT organisations.name, organisations.description, organisations.id FROM organisations 
                                                        CROSS JOIN users 
                                                        LEFT JOIN roles ON roles.users_id = users.id 
                                                        WHERE (organisations.id = roles.organisations_id) 
                                                        AND (users.id = roles.users_id) 
                                                        AND (roles.role = \'user\') 
                                                        AND (roles.users_id = ?)
                                                        GROUP BY roles.organisations_id');
        $stmtOrganisations->execute([$this->user->getId()]);
        $stmtSubscriptions->execute([$this->user->getId()]);
        $organisationsAssociative = $stmtOrganisations->fetchAllAssociative();
        $subscriptionsAssociative = $stmtSubscriptions->fetchAllAssociative();

        foreach($organisationsAssociative as $organisation){
            $newOrg = Organisation::constructWitIdNameDescription($organisation['id'], $organisation['name'], $organisation['description']);
            array_push($myOrganisations, $newOrg);
        }

        foreach($subscriptionsAssociative as $subscription){
            $newOrg = Organisation::constructWitIdNameDescription($subscription['id'], $subscription['name'], $subscription['description']);
            array_push($mySubscribtions, $newOrg);
        }


        $stmt = $this->db->prepare('SELECT users.first_name, users.last_name, messages.text, messages.title, messages.date, messages.filepath, organisations.name, channels.name AS channelName
                                        FROM organisations 
                                            CROSS JOIN users 
                                            LEFT JOIN roles ON roles.users_id = users.id 
                                            LEFT JOIN messages ON messages.channels_organisations_id = organisations.id
                                            LEFT JOIN channels ON messages.channels_id = channels.id
                                            WHERE roles.channels_id = messages.channels_id
                                            AND users.id = ?
                                            AND roles.role = \'user\'
                                            ORDER BY messages.date DESC LIMIT 5');
        $stmt->execute([$this->user->getId()]);
        $messagesFromDb = $stmt->fetchAllAssociative();

        //view
        echo $this->twig->render('/pages/dashboard.twig',[
            'organisations' => $myOrganisations,
            'subscriptions' => $mySubscribtions,
            'messages' => $messagesFromDb,
            'user' => $this->user,
            'authenticated' => true
        ]);
    }

}