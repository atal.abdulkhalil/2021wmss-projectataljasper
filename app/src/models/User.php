<?php


class User
{
     private int $id;
     private string $firstName;
     private string $lastName;
     private string $mail;
     private string $phone;
     private string $description;

    public function __construct()
    {
    }

    public static function constructWithIdFNameLNameMailPhone($id, $firstName, $lastName, $mail, $phone)
    {
        $obj = new User();
        $obj->id = $id;
        $obj->firstName = $firstName;
        $obj->lastName = $lastName;
        $obj->mail = $mail;
        $obj->phone = $phone;

        return $obj;
    }

    public static function constructWithIdFNameLNameMailPhoneDescription($id, $firstName, $lastName, $mail, $phone, $description)
    {
        $obj = new User();
        $obj->id = $id;
        $obj->firstName = $firstName;
        $obj->lastName = $lastName;
        $obj->mail = $mail;
        $obj->phone = $phone;
        $obj->description = $description;

        return $obj;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getName(): string
    {
        return $this->firstName ." " . $this->lastName;
    }


    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail(string $mail): void
    {
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }


}