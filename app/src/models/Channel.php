<?php


class Channel
{
    private int $id;
    private string $name;
    private string $description;
    private int $organisationId;

    /**
     * Channel constructor.
     * @param int $id
     * @param string $name
     * @param string $description
     * @param int $organisationId
     */
    public function __construct(int $id, string $name, string $description, int $organisationId)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->organisationId = $organisationId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getOrganisationId(): int
    {
        return $this->organisationId;
    }




}