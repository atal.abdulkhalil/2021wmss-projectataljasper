<?php


class Organisation
{
    private int $id;
    private string $name;
    private ?string $description;
    private ?bool $isOwner;

    public function __construct()
    {
    }

    public static function constructWitIdNameDescription(int $id, string $name, ?string $description){
        $obj = new Organisation();
        $obj->id = $id;
        $obj->name = $name;
        $obj->description = $description;
        return $obj;
    }

    public static function constructWitIdNameDescriptionIsOwner(int $id, string $name, ?string $description, ?bool $isOwner){
        $obj = new Organisation();
        $obj->id = $id;
        $obj->name = $name;
        $obj->description = $description;
        $obj->isOwner = $isOwner;
        return $obj;
    }

    /**
     * @return bool|null
     */
    public function getIsOwner(): ?bool
    {
        return $this->isOwner;
    }

    /**
     * @param bool|null $isOwner
     */
    public function setIsOwner(?bool $isOwner): void
    {
        $this->isOwner = $isOwner;
    }




    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return ?string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
}